import { toast } from 'react-toastify'

export const toastSuccess = (message) => {
    toast.success(
        <div><i className="far fa-check-square fa-2x"></i> {message}</div>, {
        position: toast.POSITION.TOP_RIGHT
    })
}
export const toastError = (message) => {
    toast.error(
        <div><i className="far fa-window-close fa-2x"></i> {message}</div>, {
        position: toast.POSITION.TOP_RIGHT
    })
}
export const toastWarn = (message) => {
    toast.warn(
        <div><i className="fas fa-exclamation-circle fa-2x"></i> {message}</div>, {
        position: toast.POSITION.TOP_RIGHT
    })
}