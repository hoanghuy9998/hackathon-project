import { React } from "react"
import HomePage from './pages/Home/index'
import SharePage from './pages/Share/index'
import ShareItemPage from './pages/Share/ShareItem'
import ConfessPage from './pages/Confess/index'
import ConfessItemPage from './pages/Confess/ConfessItem'
import NotFoundPage from './pages/NotFound/index'
import WarningPage from "./pages/Warning/index"
import WarningItemPage from "./pages/Warning/WarningItem"
import VolunteerPage from "./pages/Volunteer/index"
import PlantPage from "./pages/Plant/index"
import SharePublishPage from "./pages/Share/SharePublish"
import ConfessPublishPage from "./pages/Confess/ConfessPublish"
const routes = [
    {
        path: '/',
        exact: true,
        main: () => <HomePage />
    },
    {
        path: '/share',
        exact: true,
        // main: () => <SharePage />
        main: ({ match, location }) => <SharePage match={match} location={location} />
    },
    {
        path: '/share/publish',
        exact: true,
        main: ({ match, location }) => <SharePublishPage match={match} location={location} />
    },
    {
        path: '/share/:id',
        exact: false,
        main: ({ match, location }) => <ShareItemPage match={match} location={location} />
    },

    {
        path: '/motivation',
        exact: true,
        main: ({ match, location }) => <ConfessPage match={match} location={location} />
    },
    {
        path: '/motivation/publish',
        exact: true,
        main: ({ match, location }) => <ConfessPublishPage match={match} location={location} />
    },
    {
        path: '/motivation/:id',
        exact: false,
        main: ({ match, location }) => <ConfessItemPage match={match} location={location} />
    },
    {
        path: '/warning',
        exact: true,
        main: ({ match, location }) => <WarningPage match={match} location={location} />
    },
    {
        path: '/warning/:id',
        exact: false,
        main: ({ match, location }) => <WarningItemPage match={match} location={location} />
    },
    {
        path: '/volunteer',
        exact: false,
        main: () => <VolunteerPage />
    },
    {
        path: '/plant',
        exact: false,
        main: () => <PlantPage />
    },
    {
        path: '',
        exact: false,
        main: () => <NotFoundPage />
    }
]


export default routes;