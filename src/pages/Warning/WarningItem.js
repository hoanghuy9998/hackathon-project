import { Component } from 'react'
import History from "../../components/WarningComponent/History"

const histories = [
    {
        id: 1,
        title: "Thông báo số 1",
        date: "04/02/2012",
        img: "https://media.suckhoedoisong.vn/Images/thaibinh/2021/02/02/benh_nhan_COVID-19_benh_nen.jpg",
        patiens: [
            {
                id: 1,
                places: [
                    {
                        id: 1,
                        date: "12/03/2022",
                        commune: "Tân Phong",
                        ward: "12",
                        District: "Thủ Đức",
                        City: "Hồ Chí Minh",
                        Province: ""
                    },
                    {
                        id: 2,
                        date: "12/03/2022",
                        commune: "B",
                        ward: "12",
                        District: "Hoàn Kiếm",
                        City: "Hà Nội",
                        Province: ""
                    }
                ]
            }
        ]

    },
    {
        id: 1,
        title: "Thông báo số 2",
        date: "04/02/2012",
        img: "https://media.suckhoedoisong.vn/Images/thaibinh/2021/02/02/benh_nhan_COVID-19_benh_nen.jpg",
        patiens: [
            {
                id: 2,
                places: [
                    {
                        id: 1,
                        date: "12/05/2022",
                        commune: "Tân Phong",
                        ward: "12",
                        District: "Thủ Đức",
                        City: "Hồ Chí Minh",
                        Province: ""
                    },
                    {
                        id: 2,
                        date: "12/06/2022",
                        commune: "B",
                        ward: "12",
                        District: "Hoàn Kiếm",
                        City: "Hà Nội",
                        Province: ""
                    }
                ]
            }
        ]
    },
    {
        id: 3,
        title: "Thông báo số 3",
        date: "04/02/2012",
        img: "https://media.suckhoedoisong.vn/Images/thaibinh/2021/02/02/benh_nhan_COVID-19_benh_nen.jpg",
        patiens: [
            {
                id: 3,
                places: [
                    {
                        id: 1,
                        date: "12/09/2022",
                        commune: "Tân Phong",
                        ward: "12",
                        District: "Sad",
                        City: "An Giang",
                        Province: ""
                    },
                    {
                        id: 2,
                        date: "12/10/2022",
                        commune: "B",
                        ward: "12",
                        District: "Hoàn Kiếm",
                        City: "Hà Nội",
                        Province: ""
                    }
                ]
            }
        ]
    },
]

class WarningItemPage extends Component {
    render() {
        var { match } = this.props;
        var id = parseInt(match.params.id);
        const history = histories[(id) - 1]
        return (
            <>
                <div className="container mt-5 mb-5">
                    <History history={history} />
                </div>
            </>
        );
    }
}

export default WarningItemPage;
