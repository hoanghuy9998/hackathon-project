import { Component } from 'react'
import { toastSuccess, toastError } from "../../helper/toastFunct"
class ConfessPublishPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            author: '',
            title: '',
            text: '',
        }
    }

    onChange = (event) => {
        var target = event.target;
        var name = target.name;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
            [name]: value
        });
    }
    onSubmit = () => {
        var { author } = this.state;
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        if (this.state && (!this.state.author || !this.state.title || !this.state.text))
            toastError("Các trường không được bỏ trống")
        else if (!pattern.test(author)) {
            toastError("Email không hợp lệ")
        }
        else {
            // this.props.onSubmit(this.state);
            toastSuccess("Gửi thành công, TNV sẽ duyệt và đăng bài sớm nhất")
            window.location.href = '/share'
        }
    }
    render() {
        return (
            <>
                <div className="contact-bg">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="contactheading">
                                    <h3>Gửi bài tâm sự cho Blog</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <section className="layout_padding">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="full comment_form">
                                    <form action="index.html">
                                        <fieldset>
                                            <div className="col-md-12">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <input
                                                            type="email"
                                                            name="author" placeholder="Email"
                                                            value={this.state.author}
                                                            onChange={this.onChange}
                                                        />
                                                        <input
                                                            type="text"
                                                            name="title" placeholder="Tựa đề"
                                                            value={this.state.title}
                                                            onChange={this.onChange}
                                                        />
                                                        <textarea
                                                            className="wdt"
                                                            name="text"
                                                            value={this.state.text}
                                                            onChange={this.onChange}
                                                            placeholder="Bài viết"></textarea>
                                                    </div>
                                                </div>
                                                <div className="row margin_top_30">
                                                    <div className="col-md-12">
                                                        <div className="center">
                                                            <div
                                                                onClick={this.onSubmit}
                                                                className="btn btn primary custom-button text-center"> Gửi</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}



export default ConfessPublishPage;
