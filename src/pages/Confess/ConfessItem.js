import { Component } from 'react'
import Comment from '../../components/Comment/index'
import MyComment from '../../components/Comment/MyComment'
import Confession from '../../components/Confess/Confession'
const confessions = [
    {
        id: 1,
        title: "lorem ipsum",
        date: "04/02/2012",
        img: "https://files.benhvien108.vn/ecm/source_files/2020/04/29/200429-2-2-163041-290420-33.jpg",
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip",
        author: "abcyz@gmail.com",
        comments: [
            {
                id: 1,
                date: "10/3/2017",
                body: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreetdolore magna aliquam erat volutpatasddwqeqwesd",
                author: "Tuan@gmail.com",
                img: "https://i-english.vnecdn.net/2020/11/23/TUan1-1606123165-7014-1606123704_r_680x408.jpg",
                like: 5
            },
            {
                id: 2,
                date: "10/3/2017",
                body: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreetdolore magna aliquam erat volutpatasddwqeqwesd",
                author: "Hoang@gmail.com",
                img: "https://kenh14cdn.com/thumb_w/620/2019/1/19/vn8-1547868010313507741114.jpg",
                like: 7
            }
        ]
    },
    {
        id: 2,
        title: "lorem ipsum",
        date: "04/02/2012",
        img: "https://files.benhvien108.vn/ecm/source_files/2020/04/29/200429-2-1-163041-290420-33.jpg",
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip",
        author: "abcyz@gmail.com",
        comments: [{
            id: 1,
            date: "10/3/2017",
            body: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreetdolore magna aliquam erat volutpatasddwqeqwesd",
            author: "Hoang@gmail.com",
            img: "https://kenh14cdn.com/thumb_w/620/2019/1/19/vn8-1547868010313507741114.jpg",
            like: 7
        }]
    },
    {
        id: 3,
        title: "lorem ipsum",
        date: "04/02/2012",
        img: "https://binhphuoc.gov.vn/uploads/binhphuoc/syt/2021_02/20200306_030439_936083_covid.max-1800x1800.jpg",
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip",
        author: "abcyz@gmail.com",
        comments: []
    },
    {
        id: 4,
        title: "lorem ipsum",
        date: "04/02/2012",
        img: "https://www.fvhospital.com/wp-content/uploads/2020/04/sars-cov-2.jpg",
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip",
        author: "abcyz@gmail.com",
        comments: [
            {
                id: 1,
                date: "10/3/2021",
                body: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreetdolore magna aliquam erat volutpatasddwqeqwesd",
                author: "Vy@gmail.com",
                img: "https://kenh14cdn.com/thumb_w/620/2019/1/19/vn8-1547868010313507741114.jpg",
                like: 7
            }
        ]
    }
]

class ConfessItemPage extends Component {
    render() {
        const confession = confessions[(this.props.match.params.id) - 1]
        return (
            <>
                <div className="container mt-5 mb-5">
                    <Confession confession={confession} />
                </div>
                {/* <HomeComment /> */}
                <Comment topic={confession} />
                <MyComment url="/" />
            </>
        );
    }
}

export default ConfessItemPage;
