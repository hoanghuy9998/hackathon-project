import { Component } from 'react'
//import routes from "./routes.txt"
import Confession from '../../components/Confess/Confession'
import { NavLink } from 'react-router-dom'


const confessions = [
    {
        id: 1,
        title: "lorem ipsum",
        date: "04/02/2012",
        img: "https://files.benhvien108.vn/ecm/source_files/2020/04/29/200429-2-2-163041-290420-33.jpg",
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip",
        author: "abcyz@gmail.com",
        comments: [
            {
                id: 1,
                date: "10/3/2017",
                body: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreetdolore magna aliquam erat volutpatasddwqeqwesd",
                author: "Tuan@gmail.com",
                img: "https://i-english.vnecdn.net/2020/11/23/TUan1-1606123165-7014-1606123704_r_680x408.jpg",
                like: 5
            },
            {
                id: 2,
                date: "10/3/2017",
                body: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreetdolore magna aliquam erat volutpatasddwqeqwesd",
                author: "Hoang@gmail.com",
                img: "https://kenh14cdn.com/thumb_w/620/2019/1/19/vn8-1547868010313507741114.jpg",
                like: 7
            }
        ]
    },
    {
        id: 2,
        title: "lorem ipsum",
        date: "04/02/2012",
        img: "https://files.benhvien108.vn/ecm/source_files/2020/04/29/200429-2-1-163041-290420-33.jpg",
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip",
        author: "abcyz@gmail.com",
        comments: [{
            id: 1,
            date: "10/3/2017",
            body: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreetdolore magna aliquam erat volutpatasddwqeqwesd",
            author: "Hoang@gmail.com",
            img: "https://kenh14cdn.com/thumb_w/620/2019/1/19/vn8-1547868010313507741114.jpg",
            like: 7
        }]
    },
    {
        id: 3,
        title: "lorem ipsum",
        date: "04/02/2012",
        img: "https://binhphuoc.gov.vn/uploads/binhphuoc/syt/2021_02/20200306_030439_936083_covid.max-1800x1800.jpg",
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip",
        author: "abcyz@gmail.com",
        comments: []
    },
    {
        id: 4,
        title: "lorem ipsum",
        date: "04/02/2012",
        img: "https://static.ttbc-hcm.gov.vn/w815/images/upload/lienphuong/04242020/covid1-26-3.jpg",
        body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip",
        author: "abcyz@gmail.com",
        comments: [
            {
                id: 1,
                date: "10/3/2021",
                body: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreetdolore magna aliquam erat volutpatasddwqeqwesd",
                author: "Vy@gmail.com",
                img: "https://kenh14cdn.com/thumb_w/620/2019/1/19/vn8-1547868010313507741114.jpg",
                like: 7
            }
        ]
    }
]
class ConfessPage extends Component {
    render() {
        var { match } = this.props;
        var url = match.url;
        let result = confessions.map((confession, index) => {
            return <div className="container mt-3 mb-3" key={index}>
                <NavLink to={`${url}/${confession.id}`} >
                    <Confession className="mt-3 mb-3" key={index} confession={confession} homeCheck={0} elementShow={3} />
                </NavLink>
            </div>

        })
        return (
            <>
                <div className="Marketing-bg">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="Marketingheading">
                                    <h3 className="dancing-style s-30">Tâm sự khó khăn mùa dịch</h3>
                                </div>
                            </div>
                        </div>
                        <div className="d-flex flex-row-reverse bd-highlight text-center">
                            <NavLink to="/motivation/publish">
                                <button type="button" className="btn btn-success btn-lg">Gửi bài tâm sự cho Blog</button>
                            </NavLink>
                        </div>
                    </div>
                </div>
                <div className="container">
                    {result}
                </div>
            </>
        );
    }
}


export default ConfessPage;