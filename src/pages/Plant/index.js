import { Component } from 'react'
import { toastSuccess, toastError } from "../../helper/toastFunct"
import Form from "../../components/PlantComponents/Form"
class PlantPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            elemShow: 1,
            email: ''
        }
    }
    showForm = (elemShow) => {
        let elements = [];
        for (let i = 0; i < elemShow; ++i) {
            elements.push(<Form key={i} />)
        }
        return elements;
    }
    onAdd = () => {
        this.setState({
            elemShow: this.state.elemShow + 1
        })
    }
    onChange = (event) => {
        var target = event.target;
        var value = target.value;
        this.setState({
            email: value
        });
    }
    onSubmit = () => {
        var { email } = this.state;
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        if (this.state && !this.state.email)
            toastError("Trường email không được bỏ trống")
        else if (!pattern.test(email)) {
            toastError("Email không hợp lệ")
        }
        else {
            // this.props.onSubmit(this.state);
            toastSuccess("Gửi thành công, TNV sẽ duyệt và đăng bài sớm nhất")
            window.location.href = '/'
        }
    }
    render() {
        const { elemShow } = this.state;
        return (
            <>
                <div className="contact-bg">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="contactheading">
                                    <h3 className="dancing-style s-30">Kế hoạch di chuyển của bạn</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <section className="layout_padding">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="full comment_form">
                                    <form>
                                        <fieldset>
                                            <div className="col-md-12">
                                                <div className="row">
                                                    <input type="email"
                                                        value={this.state.email}
                                                        onChange={this.onChange}
                                                        name="email" placeholder="Email"
                                                    />
                                                </div>
                                                {this.showForm(elemShow)}
                                            </div>

                                            <div className="d-flex align-items-end flex-column">
                                                <button type="button"
                                                    onClick={this.onAdd}
                                                    className="btn btn-info custom-add-form "><i className="fas fa-plus fa-2x"></i></button>
                                            </div>
                                            <div className="row margin_top_30">
                                                <div className="col-md-12">
                                                    <div className="center">
                                                        <div
                                                            onClick={this.onSubmit}
                                                            className="btn btn primary custom-button text-center" to="warning"> Gửi</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}

export default PlantPage;
