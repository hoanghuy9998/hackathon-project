import { Component } from 'react'
import '../../App.css'
import Banner from '../../components/HomeComponents/Banner/index'
import HomeShare from '../../components/HomeComponents/HomeShare/index'
import HomeConfess from '../../components/HomeComponents/HomeConfess/index'
import HomeComment from '../../components/HomeComponents/HomeComment/index';
import HomeIntro from '../../components/HomeComponents/HomeIntro';
import LazyLoad from "react-lazyload"
class HomePage extends Component {
    render() {
        return (
            <>

                <Banner />
                <LazyLoad height={200} once>
                    <HomeShare />
                </LazyLoad>
                <LazyLoad height={200} once>
                    <HomeConfess />
                </LazyLoad>
                <LazyLoad height={200} once>
                    <HomeComment />

                </LazyLoad>
                <LazyLoad height={200} once>
                    <HomeIntro />
                </LazyLoad>
            </>
        );
    }
}

export default HomePage;
