import { Component } from 'react'
class Topic extends Component {
    render() {
        const { title, body, date, img, id, author } = this.props.topic;
        const rowName = (id !== 1 ? "row margin_top_30" : "row")
        return (
            <div className={rowName}>
                <div className="col-md-6">
                    <img className="rounded" src={img} alt="#" />
                </div>
                <div className="col-md-6">
                    <div className="full blog_cont">
                        <h4>{title}</h4>
                        <h5>{date}</h5>
                        <h5>{author}</h5>
                        <p  >{body}</p>
                    </div>
                </div>
            </div>
        )
    }
}


export default Topic