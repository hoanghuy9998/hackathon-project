import { Component } from 'react'
class Form extends Component {
    render() {
        return (
            <div className="row">
                <hr className="my-4" />
                <input type="text" name="text"
                    placeholder="Địa chỉ nơi bạn đến" />
                <div className="col-md-12 text-left">
                    <p className="warning-p">*hoặc ghi địa chỉ cụ thể</p>
                </div>
                <div className="col-md-6">
                    <select id="input" className="form-control rounded-pill mt-3" placeholder="Xã">
                        <option value="">Xã</option>
                        <option value="Tân Phong">Tân Phong</option>
                        <option value="Lorem">Lorem</option>
                        <option value="Ipsum">Ipsum</option>
                    </select>
                    <select name="" id="input" className="form-control rounded-pill mt-3" placeholder="Quận/Huyện">
                        <option value="">Quận/Huyên</option>
                        <option value="Tân Phong">Tân Phong</option>
                        <option value="Lorem">Lorem</option>
                        <option value="Ipsum">Ipsum</option>
                    </select>
                </div>
                <div className="col-md-6">
                    <select name="" id="input" className="form-control rounded-pill mt-3" placeholder="Phường">
                        <option value="">Phường</option>
                        <option value="Tân Phong">Tân Phong</option>
                        <option value="Lorem">Lorem</option>
                        <option value="Ipsum">Ipsum</option>
                    </select>
                    <select name="" id="input" className="form-control rounded-pill mt-3" placeholder="Thành phố">
                        <option value="">Thành phố</option>
                        <option value="Tân Phong">Tân Phong</option>
                        <option value="Lorem">Lorem</option>
                        <option value="Ipsum">Ipsum</option>
                    </select>
                    <select name="" id="input" className="form-control rounded-pill mt-3" placeholder="Tỉnh">
                        <option value="">Tỉnh</option>
                        <option value="Tân Phong">Tân Phong</option>
                        <option value="Lorem">Lorem</option>
                        <option value="Ipsum">Ipsum</option>
                    </select>
                </div>
                <div className="row login-radio mr-3">
                    <input type="radio"
                        id="receivedEmailT"
                        name="receivedEmail"
                        value="true" />
                    <label className="ml-3" htmlFor="receivedEmailT">Đây là địa chỉ cố định (trường, công sở)</label>
                </div>
                <div className="row login-radio ml-3">
                    <input type="radio"
                        id="receivedEmailF"
                        name="receivedEmail"
                        value="false" />
                    <label className="ml-3" htmlFor="receivedEmailF">Đây là địa chỉ không cố định (khu vui chơi, mua sắm)</label><br />
                </div>
            </div>
        )
    }
}


export default Form