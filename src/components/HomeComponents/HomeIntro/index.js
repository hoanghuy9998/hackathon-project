import { Component } from 'react'

import news1 from './Asset/news1.jpg'
import news3 from './Asset/news9.jpg'
import news4 from './Asset/news4.jpg'
import news6 from './Asset/news11.jpg'

class HomeIntro extends Component {
    render() {
        return (
            <div className="container mb-5 mt-5">
                <div className="row">
                    <div className="col-md-8 offset-md-2">
                        <div className="heading">
                            <h3 className="Dancing-style s-30">Việt Nam Quyết Thắng Đại Dịch</h3>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-8 offset-md-2">
                        <div className="full">
                            <div className="big_blog">
                                <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                                    <ol className="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators" data-slide-to={0} className="active" />
                                        <li data-target="#carouselExampleIndicators" data-slide-to={1} />
                                        <li data-target="#carouselExampleIndicators" data-slide-to={2} />
                                        <li data-target="#carouselExampleIndicators" data-slide-to={3} />
                                    </ol>
                                    <div className="carousel-inner fixed rounded">
                                        <div className="carousel-item active">
                                            <img className="d-block w-100" src={news1} alt="First slide" />
                                        </div>
                                        <div className="carousel-item">
                                            <img className="d-block w-100" src={news3} alt="Second slide" />
                                        </div>
                                        <div className="carousel-item">
                                            <img className="d-block w-100" src={news4} alt="Third slide" />
                                        </div>
                                        <div className="carousel-item">
                                            <img className="d-block w100" src={news6} alt="Fourd slide" />
                                        </div>
                                    </div>
                                    <a className="carousel-control-prev" role="button" data-slide="prev" href="#carouselExampleIndicators">
                                        <span className="carousel-control-prev-icon" aria-hidden="true" />
                                        <span className="sr-only">Previous</span>
                                    </a>
                                    <a className="carousel-control-next" role="button" data-slide="next" href="#carouselExampleIndicators">
                                        <span className="carousel-control-next-icon" aria-hidden="true" />
                                        <span className="sr-only">Next</span>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default HomeIntro