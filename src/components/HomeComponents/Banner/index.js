import { Component } from 'react'

import poster1 from './Asset/poster5.jpg'
import poster5 from './Asset/poster1.png'
import poster2 from './Asset/poster2.jpg'
import poster3 from './Asset/poster3.jpg'
import poster4 from './Asset/poster4.jpg'
import { toastSuccess } from '../../../helper/toastFunct'

class Banner extends Component {
    onClick = () => {
        navigator.clipboard.writeText('https://app-covidblog.herokuapp.com/')
        toastSuccess("đã copy vào clipboard")
    }
    onRedirect = () => {
        window.location.href = '/volunteer'
    }
    render() {
        return (
            <div className="banner-slider">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-7">
                            <div id="slider_main" className="carousel slide" data-ride="carousel">
                                <div className="carousel-inner">
                                    <div className="carousel-item active">
                                        <img src={poster1} alt="#" />
                                    </div>
                                    <div className="carousel-item">
                                        <img src={poster2} alt="#" />
                                    </div>
                                    <div className="carousel-item">
                                        <img src={poster3} alt="#" />
                                    </div>
                                    <div className="carousel-item">
                                        <img src={poster4} alt="#" />
                                    </div>
                                    <div className="carousel-item">
                                        <img src={poster5} alt="#" />
                                    </div>
                                </div>
                                <a className="carousel-control-prev" href="#slider_main" data-slide="prev">
                                    <i className="fa fa-angle-left" aria-hidden="true" />
                                </a>
                                <a className="carousel-control-next" href="#slider_main" data-slide="next">
                                    <i className="fa fa-angle-right" aria-hidden="true" />
                                </a>
                            </div>
                        </div>
                        <div className="col-md-5">
                            <div className="full slider_cont_section">
                                <h4 className="dancing-style s-30">Một trang blog vì</h4>
                                <h3 className="dancing-style s-30">cộng đồng</h3>
                                <ol className="text-left">
                                    <li>So khớp địa chỉ của các ca mắc và tiến hành thông báo qua email và các trang mạng xã hội khác</li>
                                    <li>Cung cấp các chia sẻ, kinh nghiệm phòng chống và góc nhìn rõ hơn về Covid từ cộng đồng</li>
                                    <li>Nhận được sự chia sẻ từ cộng đồng khi không may mắc Covid hoặc gặp khó khăn do dịch Covid tác động</li>
                                </ol>
                                <div className="button-banner">
                                    <button
                                        onClick={this.onClick}
                                        type="button" className="btn 
                                    btn-info btn-lg">
                                        Chia sẻ trang
                                    </button>
                                    <button
                                        onClick={this.onRedirect}
                                        type="button" className="btn 
                                    btn-info btn-lg">
                                        Đăng ký làm TNV duyệt bài
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default Banner