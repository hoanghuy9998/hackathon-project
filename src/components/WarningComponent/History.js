import { Component } from 'react'


class Histories extends Component {
    showPatents = (patients) => {
        let result = null;
        result = patients.map((patient, index) => {
            const { places } = patient;
            return (
                <div key={index}>
                    <p>Bệnh nhân số: {patient.id} </p>
                    {places.map((place, index) => {
                        let Province = place.Province === "" ? "" : `/${place.Province}`
                        let City = place.City === "" ? "" : `/${place.City}`
                        return <p key={index}>{place.commune}/{place.ward}/{place.District}{City}{Province}</p>
                    })}
                </div>
            )
        })
        return result;
    }
    render() {
        const { patiens, date, title, img, id } = this.props.history;
        const rowName = (id !== 1 ? "row margin_top_30" : "row")
        return (
            <div className={rowName}>
                <div className="col-md-6">
                    <img src={img} alt="#" />
                </div>
                <div className="col-md-6">
                    <div className="full blog_cont">
                        <h4>{title}</h4>
                        <h5>Ngày {date}</h5>
                        {this.showPatents(patiens)}
                    </div>
                </div>
            </div>
        )
    }
}


export default Histories