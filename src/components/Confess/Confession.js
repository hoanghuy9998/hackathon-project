import { Component } from 'react'

class Confession extends Component {
    render() {
        const { img, date, body, title, author } = this.props.confession;
        const homeCheck = this.props.homeCheck;
        const border = homeCheck !== 1 ? "border" : ""
        const text = homeCheck === 1 ? (
            <div className="full blog_cont">
                <h3 className="white_font">{title}</h3>
                <h5 className="grey_font">{date}</h5>
                <p className="white_font" >{body}</p>
            </div>) :
            (<div className="full blog_cont" >
                <h3>{title}</h3>
                <h5>{date}</h5>
                <h5>{author}</h5>
                <p >{body}</p>
            </div >)
        return (
            <div className={`"section layout_padding"${border}`}>
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <img className="rounded" src={img} alt="#" />
                        </div>
                        <div className="col-md-6">
                            <div className="full blog_cont">
                                {text}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



export default Confession;