import { Component } from 'react'

import YourComment from './YourComment.js'



class Comment extends Component {
    render() {
        var comments = this.props.topic.comments
        return (
            <section className="layout_padding">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="heading" style={{ paddingLeft: '15px', paddingRight: '15px' }}>
                                <h4 style={{ borderBottom: 'solid #333 1px' }}>Bình luận</h4>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <YourComment comments={comments} />
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}


export default Comment

