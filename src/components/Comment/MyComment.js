import { Component } from 'react'
import { NavLink } from "react-router-dom"
import { toastSuccess } from "../../helper/toastFunct"
class MyComment extends Component {
    render() {
        const { url } = this.props
        return (
            <>
                <div className="row margin_top_30">
                    <div className="col-md-12 margin_top_30">
                        <div className="heading" style={{ paddingLeft: '15px', paddingRight: '15px' }}>
                            <h4>Tham gia bình luận</h4>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="full comment_form">
                            <form action="index.html">
                                <fieldset>
                                    <div className="col-md-12">
                                        <div className="row">
                                            <div className="container">
                                                <input type="email" name="email" placeholder="Email" required="#" />
                                                <textarea placeholder="Comment" />
                                            </div>
                                        </div>
                                        <div className="row margin_top_30 mb-3">
                                            <div className="col-md-12">
                                                <div className="center">
                                                    <NavLink
                                                        onClick={() => toastSuccess("Gửi thành công")}
                                                        className="btn btn primary custom-button text-center" to={url}> Gửi</NavLink>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}


export default MyComment

