import { Component } from 'react'





class YourComment extends Component {
    render() {
        // const { acomments } = this.props
        var { comments } = this.props
        if (!comments)
            return (
                <div className="full comment_blog_line">
                    <h1>Không có bình luận</h1>
                </div>
            )
        let result = comments.map((comment, index) => {
            const { img, author, date, body, like } = comment;
            return (<div className="row mb-3 border comment-rounded" key={index}>
                <div className="col-md-1 pt-3">
                    <img className="rounded-circle w-75 h-75 text-center" src={img} alt="#" />
                </div>
                <div className="col-md-9">
                    <div className="full contact_text text-left pt-3">
                        <h3>{author}</h3>
                        <h4>{date}</h4>
                        <p>{body}</p>
                    </div>
                </div>
                <div className="col-md-2 pt-3">
                    <button type="button" className="btn btn-danger"><i className="fas fa-heart"></i></button>
                    <p>{like}</p>
                </div>
            </div>)
        })
        return (
            <div className="full comment_blog_line ">
                {result}
            </div>
        )
    }
}


export default YourComment