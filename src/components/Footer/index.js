import { Component } from 'react'

import footer1 from "./Asset/footer1.jpg"
import footer2 from "./Asset/footer2.jpg"
import footer3 from "./Asset/footer3.jpg"
import footer4 from "./Asset/footer4.png"
import news3 from "./Asset/news3.png"
import news6 from "./Asset/news6.jpg"
import logo from "./Asset/logo.png"
import logoTeam from "./Asset/sm_team_logo.png"
class HomeFooter extends Component {
    render() {
        return (
            <footer>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4 col-md-6 text-left" >
                            <img className="w-35 h-25 pl-0 rounded mr-5" src={logo} alt="#" />
                            <img className="w-35 h-25 pl-0 rounded mr-5" src={logoTeam} alt="#" />
                        </div>
                        <div className="col-lg-8 col-md-8">
                            <div className="container">
                                <div className="row">
                                    <div className="footer_links">
                                        <h3 className="Dancing-styles">Việt Nam thời Covid</h3>
                                    </div>
                                    <div className="row">
                                        <div className="col">
                                            <img className="img-responsive" src={footer1} alt="#" />
                                        </div>
                                        <div className="col">
                                            <img className="img-responsive" src={footer2} alt="#" />
                                        </div>
                                        <div className="col">
                                            <img className="img-responsive" src={news3} alt="#" />
                                        </div>
                                        <div className="w-100"> </div>
                                        <div className="col">
                                            <img className="img-responsive" src={footer3} alt="#" />
                                        </div>
                                        <div className="col">
                                            <img className="img-responsive" src={footer4} alt="#" />
                                        </div>
                                        <div className="col">
                                            <img className="img-responsive" src={news6} alt="#" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </footer >

        )
    }
}


export default HomeFooter


