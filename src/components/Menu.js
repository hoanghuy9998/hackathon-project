import { Component } from 'react'
import { Route, Link } from 'react-router-dom'

// should create an array for a menu (object)
const menus = [
    {
        name: "Trang chủ",
        to: "/",
        exact: true
    },
    {
        name: "Cảnh báo",
        to: "/warning",
        exact: false
    },
    {
        name: "Chia sẻ kinh nghiệm",
        to: "/share",
        exact: false
    },
    {
        name: "Tâm sự mùa dịch",
        to: "/motivation",
        exact: false
    },
    {
        name: "Kế hoạch di chuyển của bạn",
        to: "/plant",
        exact: false
    }
]


const MenuLink = ({ label, to, activeOnlyWhenExact }) => {
    return (
        // true --> exact will turn on
        <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => {
            // match -> className = active
            var active = match ? 'active' : '';
            return (
                // double className
                //<li className={'my-li ${active}'} ></li>
                <li className={active} >
                    <Link
                        to={to} className="my-link">
                        {label}
                    </Link>
                </li>
            )
        }} />
    )
}


class Menu extends Component {
    render() {
        return (
            <div className="menu-area">
                <div className="limit-box">
                    <nav className="main-menu">
                        <ul className="menu-area-main">
                            {this.showMenu(menus)}
                        </ul>
                    </nav>
                </div>
            </div>
        )
    }
    showMenu = (menus) => {
        var result = null;
        if (menus.length > 0) {
            result = menus.map((item, index) => {
                return (
                    <MenuLink key={index} label={item.name} to={item.to} activeOnlyWhenExact={item.exact} />
                )
            })
        }
        return result;
    }
}


export default Menu