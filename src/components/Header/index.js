import { Component } from 'react'
import Menu from '../Menu'
import Logo from '../Logo/index'
import gif from './Asset/loading3.gif'

class Header extends Component {
    render() {
        return (
            <>
                <div className="loader_bg">
                    <div className="loader">
                        <img src={gif} alt="" />
                    </div>
                </div>
                <header>
                    <div className="container-fluid">
                        <div className="row">
                            <Logo />
                            <div className="col-lg-9">
                                <Menu />
                            </div>
                        </div>
                    </div>
                </header>
            </>
        )
    }
}


export default Header












