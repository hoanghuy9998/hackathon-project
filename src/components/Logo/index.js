import { Component } from 'react'
import { NavLink } from "react-router-dom"
import logo from './Asset/sm-logo2.png'

class Logo extends Component {
    render() {
        return (
            <div className="col-lg-3 logo_section">
                <div className="full">
                    <div className="center-desk">
                        <NavLink to="/">
                            <div className="row ml-3">
                                <div className="logo ml-5"><img src={logo} alt="#" /></div>
                                <div className="col mr-5 mt-4 title">
                                    <h1>We Talk</h1>
                                    <h1>About Covid</h1>
                                </div>
                            </div>
                        </NavLink>
                    </div>
                </div>
            </div>
        )
    }
}


export default Logo