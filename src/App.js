import { Component } from 'react'
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Footer from './components/Footer/index';
import Header from './components/Header/index';
import routes from "./routes.js"
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
class App extends Component {
  render() {
    return (
      <Router >
        <div className="App">
          <div>
            <Header />
            <Switch>
              {this.showContentMenus(routes)}
            </Switch>
            <Footer />
            <ToastContainer
              position="top-right"
              autoClose={4500}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable />
          </div >
        </div >
      </Router >
    );
  }
  showContentMenus = (routes) => {
    var result = null;
    if (routes.length > 0) {
      result = routes.map((route, index) => {
        return (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.main} />
        )
      })
    }
    return result;
  }
}

export default App;
